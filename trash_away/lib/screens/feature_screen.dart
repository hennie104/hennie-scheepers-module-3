import 'package:flutter/material.dart';

void main(List<String> args) {
  runApp(const FeatureScreen());
}

class FeatureScreen extends StatefulWidget {
  const FeatureScreen({Key? key}) : super(key: key);

  @override
  State<FeatureScreen> createState() => _FeatureScreenState();
}

class _FeatureScreenState extends State<FeatureScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            appBar: AppBar(
          centerTitle: true,
          title: const Text('Feature Screen'),
          backgroundColor: Colors.teal,
        )));
  }
}
